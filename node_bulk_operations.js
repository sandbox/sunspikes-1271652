Drupal.behaviors.nbo = function() {
	// Onchange manipulate dropdowns
	$('#edit-from-content-type').change(function () {
		nbo_set_from_userrefs();
	});
	$('#edit-to-content-type').change(function () {
		nbo_set_to_userrefs();
	});
	
	function nbo_set_from_userrefs() {
		nbo_clear_dropdown($('#edit-from-userref option'));
		nbo_populate_dropdown($('#edit-from-content-type').val(), $('#edit-from-userref'));
	}
	function nbo_set_to_userrefs() {
		nbo_clear_dropdown($('#edit-to-userref option'));
		nbo_populate_dropdown($('#edit-to-content-type').val(), $('#edit-to-userref'));
	}
	
	// Clear dropdown options
	function nbo_clear_dropdown(elms) {
		elms.each(function() { $(this).remove(); });
	}
	
	// Populate curresponding options for the contenttype
	function nbo_populate_dropdown(type, elm) {
		for (i in Drupal.settings.userref[type]) {
			elm.append('<option value="' + i + '">' + i + '</option>');
		}
	}
};