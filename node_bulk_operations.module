<?php
/**
 * @file
 * Automatically create and maintain multiple nodes from a template node.
 */

/**
* Implementation of hook_action_info().
*/
function node_bulk_operations_action_info() {
  return array(
    'node_bulk_modify_action' => array(
      'description' => t('Modify nodes as bulk'),
      'type' => 'node',
      'configurable' => TRUE,
      'hooks' => array(
        'nodeapi' => array('delete', 'insert', 'update'),
        ),
    ),
  );
}

/**
 * Action: Bulk modify nodes
 */
function node_bulk_modify_action(&$object, $context = array()) {
  
  if(isset($context['from_content_type']) && $context['from_content_type'] == $object->type) {
    // On creation of new template node 
    if($context['op'] == 'insert') {
      $from_userref = $context['from_userref'];
      
      if($from_userref != '' && isset($object->{$from_userref}) && is_array($object->{$from_userref})) {
        foreach($object->{$from_userref} as $user) {
          $to_userref = $context['to_userref'];
          
          // Create node
          $node = $object;
          $node->nid = NULL;
          $node->vid = NULL;
          $node->tnid = NULL;
          $node->created = NULL;
          $node->menu = NULL;
          $node->type = $context['to_content_type'];
          
          if (isset($node->book['mlid'])) {
            $node->book['mlid'] = NULL;
            $node->book['has_children'] = 0;
          }
          
          $node->path = NULL;
          $node->files = array();
          unset($node->{$from_userref});
          $node->{$to_userref}[0] = $user;
          $node->log = $context['from_content_type'];
          node_save($node); // Save this node      
        }
      }
    }
    
    // On deletion of a template node 
    elseif ($context['op'] == 'delete') {
      $result = db_query("SELECT nid FROM {node} WHERE type='%s'", $context['to_content_type']);
      
      while($node = db_fetch_object($result)) {
        node_delete($node->nid);
      }
    }
    
    // On edit of a template node 
    elseif ($context['op'] == 'update') {
      $result = db_query("SELECT nid, type FROM {node} WHERE type='%s'", $context['to_content_type']);
      $to_userref = $context['to_userref'];
      $from_userref = $context['from_userref'];
      
      while($node = db_fetch_object($result)) {
        $node = node_load($node->nid);
        if($node->log == $object->type) {
          $node->title = $object->title;
          $node->body = $object->body;
          $node->changed = NULL;
          foreach($object as $key => $value) {
            if(preg_match('/^field_/', $key)) {
              if($key != $to_userref && $key != $from_userref) {
                $node->{$key} = $object->{$key};
              }
            }
          }
          node_save($node); // Save this node
        }      
      }
    }
  }
}

/**
 * Bulk modify action settings form 
 */
function node_bulk_modify_action_form($context) {
  $content = content_types();
  $list = array();
  $types = array();
  $ctype = array();
  
  foreach($content as $key => $value) {
    $list[$key] = $value['name'];
    foreach($value['fields'] as $name => $field) {
      if($field['type'] == 'userreference') {
        $types[$name] = $name;
        $ctype['userref'][$key][$name] = $name;
      }
    }
  }
  
  if(count($list) > 0) {
    drupal_add_js($ctype, 'setting', 'header');
    drupal_add_js(drupal_get_path('module', 'node_bulk_operations') . '/node_bulk_operations.js', 'module', 'header');
    $form['from_content_type'] = array(
      '#title' => t('From Content Type'),
      '#type' => 'select',
      '#description' => t('The content type which should be used as the template.'),
      '#options' => $list,
      '#default_value' => isset($context['from_content_type']) ? $context['from_content_type'] : '',
      '#required' => TRUE,
    );
    $form['from_userref'] = array(
      '#title' => t('From User Reference'),
      '#type' => 'select',
      '#description' => t('The from user reference field.'),
      '#options' => $types,
      '#default_value' => isset($context['from_userref']) ? $context['from_userref'] : '',
    );
    $form['to_content_type'] = array(
      '#title' => t('To Content Type'),
      '#type' => 'select',
      '#description' => t('The content type which should be used as the target.'),
      '#options' => $list,
      '#default_value' => isset($context['to_content_type']) ? $context['to_content_type'] : '',
      '#required' => TRUE,
    );
    $form['to_userref'] = array(
      '#title' => t('To User Reference'),
      '#type' => 'select',
      '#description' => t('The to user reference field.'),
      '#options' => $types,
      '#default_value' => isset($context['to_userref']) ? $context['to_userref'] : '',
    );
  }
  else {
    $form['to_userref'] = array(
      '#type' => 'markup',
      '#value' => '<h3 style="color: red;">Sorry, there are no content types available</h3>',
    );
  }
  
  return $form;
}

/**
 * Bulk modify action form submit 
 */
function node_bulk_modify_action_submit($form, $form_state) {
  return array(
    'from_content_type' => $form_state['values']['from_content_type'],
    'from_userref' => $form_state['values']['from_userref'],
    'to_content_type' => $form_state['values']['to_content_type'],
    'to_userref' => $form_state['values']['to_userref'],
  );
}