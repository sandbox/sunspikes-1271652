Node Bulk Operations

Automatically create and maintain multiple nodes from a template node.

This module will help to create and maintain a set of nodes corresponds to a set of users from a template node of a particular content type.
Whatever change made to the template node will also get reflected in the nodes created from the template node.

This module depends on trigger and userreference (cck) modules.

Installation:

1. Install and enable the module 
2. Go to Site Configuration > Actions
3. Add action �Modify nodes as bulk�
4. Configure the action
5. Go to Site Building > Triggers
Add trigger �Modify nodes as bulk� for after saving new post, after saving updated post and after deleting a post